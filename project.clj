(defproject twitch-custom-notify "0.1.0-SNAPSHOT"
  :description "twitch webhook machine go brr"
  :url "http://example.com/FIXME"
  :license {:name "AGPL-3.0"
            :url "https://www.gnu.org/licenses/agpl-3.0.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
  				 [clj-http "3.10.3"]
  				 [org.clojure/data.json "1.0.0"]
  				 [cheshire "5.10.0"]
  				 [http-kit "2.5.0"]
  				 [compojure "1.6.2"]
  				 [clj-toml "0.3.1"]
  				 [ahungry/xdg-rc "0.0.4"]
  				]
  :repl-options {:init-ns twitch-custom-notify.core})
