(ns twitch-custom-notify.core
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]
            [org.httpkit.server :refer [run-server]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [xdg-rc.core :as xdg-rc])
  (:use [slingshot.slingshot :only [throw+ try+]] [clojure.string :only [trim]] [clj-toml.core]))

(def client-id "a6cnegfp66v0zc3pa3lx7asf18e1do")

(defn creds []
  "get bearer token from twitch."
  (def param {:client_id client-id :client_secret (trim (slurp "secrets")) :grant_type "client_credentials"})
  (def result (client/post "https://id.twitch.tv/oauth2/token"
                           {:query-params param :throw-entire-message? true :debug-body true}))
  (json/read-str (get result :body) :key-fn keyword))

(defn save-creds []
  "save and return creds"
  (let [c (creds)]
    (spit (with-xdg "twitch.notifier.clj" "creds.json") c)
    c))
(defn get-accounts [] (get (parse-string (slurp (with-xdg "twitch.notifier.clj" "accounts.toml"))) "logins"))

(defn make-hook [callback-url user-id lease-seconds]
  (def access-token (:access_token (creds)))
  (client/post "https://api.twitch.tv/helix/webhooks/hub" 
  {:oauth-token access-token :headers {:client-id client-id} 
  :throw-entire-message? true :debug-body true :debug true 
  :form-params {:hub.callback callback-url :hub.mode "subscribe" 
  :hub.topic (str "https://api.twitch.tv/helix/streams?user_id=" user-id) 
  :hub.lease_seconds lease-seconds} :content-type :json}))

(defn uid-to-login [uid] 
(def result (client/get "https://api.twitch.tv/helix/users" 
	:oauth-token access-token 
	:form-params {:id uid}))
(:login (json/read-str (:body result) :key-fn keyword)))

(defn discord-hook [] (client/post "https://discord.com/api/webhooks/" 
	{:form-params {:message "twonch streaming happening"}}))

(defn exec-hook [service] (= service "discord" (discord-hook)))

(defroutes app
  (GET "/" [] "<a href='https://0xacab.org/jrabbit/twitch-custom-notify'><h1>twitch clojure app</h1></a>")
  (POST "/hooks/:id" [] "")
  (GET "/hooks/:id" [req] )
)
  
; (run-server app {:port 8080})
